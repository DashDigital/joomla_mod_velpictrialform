<?php
/**
 * Velpic API Helper Class
 * 
 * @package    DashDigital.VelpicAPI
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 * @link       http://www.dashdigital.com.au
 * mod_helloworld is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modVelpicTrialFormHelper
{

    public static function getTimeZones(){
        $tzdata = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        $tzlist = array();
        $html = '';

        // Create the sort
        foreach($tzdata as $value){
            $dateTimeZone = new DateTimeZone($value);
            $dateObject = new DateTime("now", $dateTimeZone);
            $diff = timezone_offset_get($dateTimeZone, $dateObject);
            
            $newTz = new modVelpicTrialFormTzData($value, $diff);
            // Add this TZ to the list
            array_push($tzlist,$newTz);            
        }

        // First sort alphabetically
        usort($tzlist, function($a, $b)
        {
            return strcmp($a->name, $b->name);
        });              

        // Create the select options!
        foreach($tzlist as $tz) {
            $html .= '<option value="'.$tz->name.'"';
            $html .= ' x-tz-offset="'.$tz->offset.'"';
            // Default to UTC incase the JQuery fails
            if( $tz->name == 'UTC' ) { $html .= ' SELECTED'; }
            $html .= '>'.$tz->name.' (GMT'.$tz->offsetfriendly.') </option>';
        }

        return $html;
    }

    # Called using the Ajax component on the front-end
    public static function createTrialAjax(){
        // Required objects 
        $input = JFactory::getApplication()->input; 
        $payloadvalues = new modVelpicTrialFormAPIPayload($input);
        $json = json_encode($payloadvalues);


        $module = JModuleHelper::getModule('mod_velpictrialform');
        $params = new JRegistry($module->params);        
        $apiendpoint = $params->get('apiendpoint', '');

        // Setup cURL
        $ch = curl_init($apiendpoint);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => $json
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }        

        return $response;
        # Simulate an API call
        #sleep(5);
        #return '{ "autoLoginUrl": "http://www.getservekeep.com/wp-content/uploads/2013/03/It-worked-graphic.001.jpg" }';
    }

    # Called using AJAX on the front-end to try and guess the user's timezone
    # This needs to be replaced by our own hosted copy eventually from http://freegeoip.net, when Velpic get's too popular
    public static function getTimezoneAjax(){
        # Get the requester's IP
        $clientIP = "";
        # Need to check for the ELB for production servers
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $clientIP = $_SERVER['REMOTE_ADDR'];
        }  

        if( $clientIP == "127.0.0.1" || $clientIP == "::1" ){
            # Debug so just use the Dash Office Address
            $clientIP = "116.212.207.242";
        }

        $url = "http://freegeoip.net/json/".$clientIP;
        # http://freegeoip.net/json/8.8.8.8
        // Setup cURL
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => TRUE
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die;
        } else {
            return $response;
        } 
    }
}
class modVelpicTrialFormTzData{
    public $name;
    public $offset;
    public $offsetfriendly;
    function __construct($_name, $_offset) {
        $this->name = $_name;
        $this->offset = $_offset/60/60;

        $hours = floor($_offset / 60 / 60 );
        $minutes = ($_offset/60) % 60;

        // Make minutes positive
        if( $minutes < 0 ) { $minutes *= -1; }

        if( $_offset < 0 ) {
            $this->offsetfriendly =  sprintf("%03d", $hours) .  sprintf("%02d", $minutes);
        } else {
            $this->offsetfriendly =  sprintf("%02d", $hours) .  sprintf("%02d", $minutes);
        }

        if ( $_offset > -1 ) { $this->offsetfriendly = "+".$this->offsetfriendly; }
    }
}
class modVelpicTrialFormAPIPayload{
    public $firstName;
    public $lastName;
    public $companyName;
    public $userName;
    public $phone;
    public $email;
    public $password;
    public $timeZone;
    // Load the object from the JSON encoded string
    function __construct($input){
        $this->firstName = $input->getString('firstname');
        $this->lastName = $input->getString('lastname');
        $this->companyName = $input->getString('company');
        $this->phone = $input->getString('phonenumber');
        $this->email = $input->getString('email');
        $this->password = $input->getString('password');
        $this->timeZone = $input->getString('tzselected');
        $this->userName = $input->getString('username');
    }    
}