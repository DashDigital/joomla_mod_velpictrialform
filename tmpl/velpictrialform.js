$(document).ready(function($) {
	
	// Default the timezone
	var offset = new Date().getTimezoneOffset();	
	offset = offset/60;
	offset = offset*-1; // Invert the offest as it's not the same as server side
	$(".velpictrialformtz option[x-tz-offset="+offset+"]:first").prop('selected', true) 

	// Attempt to get the timezone from the API
	$.ajax ({                                                          
	        type    : "GET",
	        url     : "index.php?option=com_ajax&module=velpictrialform&method=getTimezone&format=json",
	        success : function(response) {
	        	if( typeof response["data"] !== 'undefined' ){
		        	var data = JSON.parse(response["data"])
		        	tz = data["time_zone"]
		        	if($(".velpictrialformtz option[value='"+tz+"']").length)
		        		$(".velpictrialformtz option[value='"+tz+"']:first").prop('selected', true) 
	        	}
	        }
	});	 	

	// Hide the divs
	$("#velpictrialform-loading").hide()
	$("#velpictrialform-completed").hide()
	$("#velpictrialform-error").hide()
		

	// Contact form test	
	var test = [
		"1 + 2 = ?",
		"2 + 3 = ?",
		"10 + 4 = ?",
		"5 + 5 = ?",
		"2 + 2 = ?",
		"8 + 1 = ?",
		"11 + 1 = ?",
		"10 + 5 = ?",
		"18 + 1 = ?",
		"15 + 2 = ?",
		"13 + 3 = ?",
		"20 + 1 = ?",
		"23 + 2 = ?",
		"26 + 1 = ?",
		"4 + 4 = ?",
		"3 + 3 = ?",
		"20 + 3 = ?",
		"24 + 4 = ?",
		"27 + 2 = ?",
		"18 + 2 = ?"
	];
	
	var answer = [
		"3",
		"5",
		"14",
		"10",
		"4",
		"9",
		"12",
		"15",
		"19",
		"17",
		"16",
		"21",
		"25",
		"27",
		"8",
		"6",
		"23",
		"28",
		"29",
		"20"
	];
	
	generate_test();
	$('#mod_velpictrialform #jsTest').show();
	$('#mod_velpictrialform #Test').val($('#mod_velpictrialform #TestAnswer').val());
	
	function generate_test() {		
		window.num = 0;
		num = Math.round(Math.random() * 19);
		
		question = "Prove you are not a robot: " + test[num] + " *";
		$('#mod_velpictrialform #jsTest').attr('placeholder',question);
	}
	
	$('#mod_velpictrialform #Submit').click( function() {
		validation_error = 0;
		
		if ($('#mod_velpictrialform #jsTest').val() != answer[num]) {
			$('#mod_velpictrialform #jsTest').addClass('rsform-error');
			$('.rsform-block-test .validation span').removeClass('formNoError');
			$('.rsform-block-test .validation span').addClass('formError');
			generate_test();
			$('#mod_velpictrialform #jsTest').val('');
			validation_error = 1;
		}
				

		$('.rsform-block').each(function() {
			if (($(this).find($('.rsform-input-box.required')).val() == '') || ($(this).find($('.rsform-text-box.required')).val() == '') || ($(this).find($('.rsform-select-box.required')).val() == '') || ($(this).find($('.rsform-upload-box.required')).val() == '')) {
				$(this).find($('.validation span')).removeClass('formNoError');
				$(this).find($('.validation span')).addClass('formError');
				validation_error = 1;
			}
			else {
				$(this).find($('.validation span')).removeClass('formError');
				$(this).find($('.validation span')).addClass('formNoError');
				
			}
		});
		
		
		// check email
		var email_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (!(email_validation.test($('#Email').val()))) {
			$('.rsform-block-email .validation span').removeClass('formNoError');
			$('.rsform-block-email .validation span').addClass('formError');
			validation_error = 1;
		}
		else {
			$('.rsform-block-email .validation span').removeClass('formError');
			$('.rsform-block-email .validation span').addClass('formNoError');
		}
		
		// check match password 
	    if ($('#password').val() == $('#c_password').val()) {
	        $('.rsform-block-c-password .validation span').removeClass('formError');
			$('.rsform-block-c-password .validation span').addClass('formNoError');
	    } 
	    else {
	        $('.rsform-block-c-password .validation span').removeClass('formNoError');
			$('.rsform-block-c-password .validation span').addClass('formError');
			validation_error = 1;
		}
		
			
		// check radio button and checkbox
		$('.form-group-wrap.required').each(function() {
			input_name = $(this).find($('input')).attr('name');
			input_checked = $('input[name="'+input_name+'"]:checked').length;
			
			if (!input_checked) {
				$(this).parent().find($('.validation span')).removeClass('formNoError');
				$(this).parent().find($('.validation span')).addClass('formError');
				validation_error = 1;
			}
			else {
				$(this).parent().find($('.validation span')).removeClass('formError');
				$(this).parent().find($('.validation span')).addClass('formNoError');
			}
		});

		
		if (validation_error) {
			return false;
		}
		else {
			// Track button click
			if( typeof ga  !== 'undefined' )
				ga('send', 'pageview', '/free-trial/process.html')
	
			$(".contact-form").hide()
			$("#velpictrialform-loading").show()
			$("#tzhidden").val($(".velpictrialformtz").val())
	        var value   =  JSON.stringify($($('#mod_velpictrialform input[class!="noAjax"]'), $('#mod_velpictrialform')).serialize());
			$.ajax ({                                                          
			        type    : "POST",
			        url     : "index.php?option=com_ajax&module=velpictrialform&method=createTrial&format=json",
			        data    : value.replace(/\"/g, ""),
			        success : function(response) {
		                response = JSON.parse(response["data"]); 
		                // Check if there was an error or not
		                if( typeof response["autoLoginUrl"]  !== 'undefined'  ) {
				            $("#trial-msg").html(response['data']);
				            $("#velpictrialform-loading").hide()
				            $("#velpictrialform-completed").show()
							// Report the redirect
							if( typeof ga  !== 'undefined'  )
								ga('send', 'pageview', '/free-trial/complete.html')
	
				            setTimeout(function () {
							   $("#velpictrialform-completed .trigger").toggleClass("drawn"); 
							}, 100);
	       
							setTimeout(function () {
							   window.location.href = response["autoLoginUrl"]; 
							}, 2000); 		   
						} else {
							$("#velpictrialform-error").text(response["reason"] + " ("+response["message"]+")")
							$("#velpictrialform-loading").hide()
							$("#velpictrialform-error").show()
							$(".contact-form").show()
	
							if( typeof ga  !== 'undefined'  )
								ga('send', 'pageview', '/free-trial/error.html?reason='+escape(response["reason"]+ " " +response["message"]))						
						}         
			        },
			        error   : function(jqXHR, error, errorThrown) {  
						if( typeof ga  !== 'undefined'  )
							ga('send', 'pageview', '/free-trial/api-error.html')
						$("#velpictrialform-error").text("An unknown error accord while trying to contact the Velpic system, please try again later or contact <a href='mailto:support@velpic.com'>support@velpic.com</a>")
			        }
			});	    
		}
	});	
});