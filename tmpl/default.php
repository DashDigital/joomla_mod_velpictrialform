<?php 
// No direct access
defined('_JEXEC') or die; 

$indexPath = JURI::root( false );

?>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<div class="contact-form rsform">
	<form id="mod_velpictrialform">
		<div class="rsform-block">
			<input type="text" value="" placeholder="First Name" name="firstname" class="required rsform-input-box velpictrialform-input" />
			<div class="validation">
				<span class="formNoError">Please let us know your name</span>
			</div>
		</div>
		<div class="rsform-block">
			<input type="text" value="" placeholder="Last Name" name="lastname" class="required rsform-input-box velpictrialform-input" />
			<div class="validation">
				<span class="formNoError">Please let us know your last name</span>
			</div>
		</div>
		<div class="rsform-block">
			<input type="text" value="" placeholder="Company" name="company" class="required rsform-input-box velpictrialform-input" />
			<div class="validation">
				<span class="formNoError">Please let us know your company name</span>
			</div>
		</div>
		<div class="rsform-block">
			<input type="text" value="" placeholder="Phone number" name="phonenumber" class="required rsform-input-box velpictrialform-input" />
			<div class="validation">
				<span class="formNoError">Please let us know your phone number</span>
			</div>
		</div>
		<div class="rsform-block rsform-block-email"> 
			<input type="text" value="" placeholder="Email" id="Email" name="email" class="required email rsform-input-box velpictrialform-input" />
			<div class="validation">
				<span class="formNoError">Please let us know your email</span>
			</div>
		</div>
		<div class="rsform-block">
			<input type="text" value="" placeholder="Username" name="username" class="required username rsform-input-box velpictrialform-input" />
			<div class="validation">
				<span class="formNoError">Please create your username</span>
			</div>
		</div>
		<div class="rsform-block">
			<input type="password" value="" placeholder="Password" id="password" name="password" class="required rsform-input-box velpictrialform-input" />
			<div class="validation">
				<span class="formNoError">Please create your password</span>
			</div>
		</div>
		<div class="rsform-block rsform-block-c-password">
			<input type="password" value="" placeholder="Confirm password" id="c_password" name="c_password" class="required rsform-input-box velpictrialform-input" />
			<div class="validation">
				<span class="formNoError">Unmatched passwords, please try again</span>
			</div>
		</div>
		<div class="rsform-block">
			<select name="timezone" class="required rsform-select-box velpictrialform-input velpictrialformtz">
				<?php echo $timezoneselects ?>
			</select>
			<input type="hidden" name="tzselected" id="tzhidden" />
			<div class="validation">
				<span class="formNoError">Please let us know your timezone</span>
			</div>
		</div>		
		<div class="rsform-block rsform-block-test">
			<div class="rsform-block-test-wrap">
				<input type="text" value="" size="20" name="jsTest" class="rsform-input-box required" id="jsTest" placeholder="Prove you are not a robot: 24 + 4 = ? *" >
				<input type="text" class="rsform-input-box required" placeholder="Prove you are not a robot: 10+2=? *" id="Test" name="form[Test]" size="20" value="">
				<input type="hidden" value="12" id="TestAnswer" name="form[TestAnswer]">
			</div>
			<div class="validation">
				<span class="formNoError">Please enter the correct answer</span>
			</div>
		</div>
		<div class="rsform-block">
			<div class="form-group-wrap required">
				<input id="Terms0" type="checkbox" name="terms" value="" > 
				<label for="Terms0">I have read and I agree to the Velpic’s <a target="_blank" href="/support/terms.html">Terms </a> and <a target="_blank" href="/privacy.html">Privacy</a></label>
				<div class="validation">
					<span class="formNoError">Please accept our Terms of Use and Privacy Policy</span>
				</div>
			</div>
		</div>
		
		<div id="velpictrialform-error" class="form-msg" style="display:none;"></div>
		
		<div class="rsform-block">
			<input type="button" value="Submit" id="Submit" />
		</div>
		<div id="trial-msg"></div>
	</form>
</div>
<div id="velpictrialform-loading" class="form-msg" style="display:none;">
	<h1>Please wait</h1>
	<div class="loading-icon"></div>
</div>
<div id="velpictrialform-completed" class="form-msg" style="display:none;">
	<h1>Trial Success!</h1>
	<div class="tick-icon">
		<div class="trigger"></div>
		<svg version="1.1" id="tick" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 viewBox="0 0 37 37" style="enable-background:new 0 0 37 37;" xml:space="preserve">
		 <path class="circ path" style="fill:none;stroke:#dc532b;stroke-width:1.5;stroke-linejoin:round;stroke-miterlimit:10;" d="
		M30.5,6.5L30.5,6.5c6.6,6.6,6.6,17.4,0,24l0,0c-6.6,6.6-17.4,6.6-24,0l0,0c-6.6-6.6-6.6-17.4,0-24l0,0C13.1-0.2,23.9-0.2,30.5,6.5z"
		/>
		<polyline class="tick path" style="fill:none;stroke:#dc532b;stroke-width:1.5;stroke-linejoin:round;stroke-miterlimit:10;" points="
		11.6,20 15.9,24.2 26.4,13.8 "/>
		</svg>
	</div>
	
	
	<div class="content">
		<p> Congratulations, now you have 30 days to access Velpic eLearning platform. Start creating amazing multi-media lessons today!<p>
		<p>You will shortly be automatically directed to the Velpic platform... Hang on!</p>
	</div>
</div>



<script src="/modules/mod_velpictrialform/tmpl/velpictrialform.js"></script>
