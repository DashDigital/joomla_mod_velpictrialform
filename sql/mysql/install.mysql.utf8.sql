CREATE TABLE IF NOT EXISTS `#__velpictrialform` (
	`id` int(10) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`value` text NOT NULL,
 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `#__velpictrialform` (`name`, `value`) VALUES ('apikey', 'http://internal.velpic.com.au/api/');
INSERT INTO `#__velpictrialform` (`name`, `value`) VALUES ('postinputtext', 'Please accept our terms and conditions');